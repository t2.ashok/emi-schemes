package cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class LocalCache {

    private static LocalCache localCache = new LocalCache();
    private Cache<String, String> cache = null;

    private LocalCache() {
        cache = Caffeine.newBuilder()
                .maximumSize(100000)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build();
    }

    public static LocalCache getInstance() {
        return localCache;
    }

    public void put(String key, String value) {
        cache.put(key, value);
    }

    public String get(String key) {
        return cache.getIfPresent(key);
    }

    public String get(String key, Function<String, String> function) {
        return cache.get(key, function);
    }
}
