package resources;

import cache.LocalCache;
import core.LoanProcessor;
import core.LoanProcessorImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scheduler.BankRateUpdator;
import util.JsonUtil;

import static spark.Spark.get;

public class LoanService {

    private static final Logger logger = LoggerFactory.getLogger(LoanService.class);
    private static LoanProcessor loanProcessor;
    private static LocalCache localCache;

    public static void main(String[] args) {
        localCache = LocalCache.getInstance();
        loanProcessor = new LoanProcessorImpl(); //In real project could be done through dependency injection
        BankRateUpdator.getInstance().startScheduler();
        get("/emi-schemes", (request, response) -> {
            String amount = request.queryParams("amount");
            return localCache.get(amount, k -> JsonUtil.toJson(loanProcessor.getRates(Double.parseDouble(k))));
        });
    }


}
