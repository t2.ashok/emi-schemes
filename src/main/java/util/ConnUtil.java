package util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnUtil {
    private static final Logger logger = LoggerFactory.getLogger(IOUtils.class);

    public static void close(Object... resources) {

        try {
            for (Object resource : resources) {
                if (resource instanceof Connection) {
                    Connection con = (Connection) resource;
                    con.close();
                }
                if (resource instanceof Statement) {
                    Statement stmt = (Statement) resource;
                    stmt.close();
                }
                if (resource instanceof PreparedStatement) {
                    PreparedStatement pstamt = (PreparedStatement) resource;
                    pstamt.close();
                }
            }
        } catch (SQLException ex) {
            logger.error("Error in closing connections ", ex);
        }
    }
}
