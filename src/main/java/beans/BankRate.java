package beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BankRate {

    @Getter(onMethod = @__(@JsonIgnore))
    private String bankName;
    private int months;
    private double rate;
    private double minimum_amount;

}
