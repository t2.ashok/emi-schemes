package beans;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(exclude = {"tenures"})
public class SearchResponse {

    private String bank;
    private List<BankRate> tenures;
}
