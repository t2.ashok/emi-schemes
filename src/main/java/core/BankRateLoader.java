package core;

import beans.BankRate;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import dao.BankRateDAO;
import dao.BankRateDAOImpl;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.CharSequenceReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static conf.Configuration.CONFIGURATION;

public class BankRateLoader {

    private static final Logger logger = LoggerFactory.getLogger(BankRateLoader.class);
    private static BankRateLoader bankRateLoader = new BankRateLoader();
    private BankRateDAO bankRateDAO;

    private BankRateLoader() {
        bankRateDAO = BankRateDAOImpl.getInstance();
    }

    public static BankRateLoader getInstance() {
        return bankRateLoader;
    }

    public void loadRates() {
        try {
            String content = getCSVContent();
            CSVFormat.DEFAULT.withHeader().parse(new CharSequenceReader(content)).getRecords().stream()
                    .map(r -> createNewBankRate(r)).forEach(rate ->
                    bankRateDAO.save(rate)
            );
            bankRateDAO.renameTable();
        } catch (Exception ex) {
            logger.error("Error loading bank loan rates ", ex);
        }
    }

    private String getCSVContent() throws UnirestException {
        Unirest.setTimeouts(Integer.parseInt(CONFIGURATION.getValue("csvConnectionTimeOut")),
                Integer.parseInt(CONFIGURATION.getValue("csvSocketTimeout")));
        return Unirest.get(CONFIGURATION.getValue("csvUrl")).asString().getBody();
    }

    private BankRate createNewBankRate(CSVRecord r) {
        String bankName = r.get("Lender");
        int tenure = Integer.parseInt(r.get("Tenure"));
        double interestRate = Double.parseDouble(r.get("Rate"));
        double minimumAmount = Double.parseDouble(r.get("Minimum"));
        return new BankRate(bankName, tenure, interestRate, minimumAmount);
    }

}
