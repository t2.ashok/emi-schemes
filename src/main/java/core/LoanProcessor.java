package core;

import beans.SearchResponse;

import java.util.Collection;

public interface LoanProcessor {

    Collection<SearchResponse> getRates(double minimumAmount);
}
