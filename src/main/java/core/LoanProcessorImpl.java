package core;

import beans.BankRate;
import beans.SearchResponse;
import dao.BankRateDAO;
import dao.BankRateDAOImpl;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class LoanProcessorImpl implements LoanProcessor {


    private static final String CONFIG_FILE_NAME = "emi-rates.properties";
    private static final Logger logger = LoggerFactory.getLogger(LoanProcessorImpl.class);
    private static String CSV_URL;
    private static int CONNECTION_TIMEOUT;
    private static int SOCKET_TIMEOUT;
    private List<BankRate> rates = null;
    private BankRateDAO bankRateDAO;
    private BankRateLoader bankRateLoader;

    public LoanProcessorImpl() {
        bankRateDAO = BankRateDAOImpl.getInstance();
        bankRateLoader = BankRateLoader.getInstance();
    }


    private BankRate createNewBankRate(CSVRecord r) {
        String bankName = r.get("Lender");
        int tenure = Integer.parseInt(r.get("Tenure"));
        double interestRate = Double.parseDouble(r.get("Rate"));
        double minimumAmount = Double.parseDouble(r.get("Minimum"));
        return new BankRate(bankName, tenure, interestRate, minimumAmount);
    }

    @Override
    public Collection<SearchResponse> getRates(double minimumAmount) {
        List<BankRate> rates = bankRateDAO.getRates(minimumAmount);
        Map<String, SearchResponse> result = new HashMap<>();
        rates.forEach(rate -> {
            if (result.get(rate.getBankName()) == null) {
                result.put(rate.getBankName(), createNewRate(rate));
            } else {
                result.get(rate.getBankName()).getTenures().add(rate);
            }
        });
        return result.values();
    }

    private SearchResponse createNewRate(BankRate rate) {
        SearchResponse searchResponse = new SearchResponse();
        searchResponse.setBank(rate.getBankName());
        List<BankRate> tenures = new ArrayList<>();
        tenures.add(rate);
        searchResponse.setTenures(tenures);
        return searchResponse;
    }
}
