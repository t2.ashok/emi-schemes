package scheduler.job;

import core.BankRateLoader;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static core.BankRateLoader.*;

public class RateUpdateJob implements Job {
    private static final Logger logger = LoggerFactory.getLogger(RateUpdateJob.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        logger.info("Rate update job called..");
        getInstance().loadRates();
    }
}
