package scheduler;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scheduler.job.RateUpdateJob;

import static conf.Configuration.CONFIGURATION;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class BankRateUpdator {
    private static final Logger logger = LoggerFactory.getLogger(BankRateUpdator.class);
    private static BankRateUpdator bankRateUpdator = new BankRateUpdator();

    private BankRateUpdator() {
    }

    public static BankRateUpdator getInstance() {
        return bankRateUpdator;
    }

    public void startScheduler() {
        try {
            SchedulerFactory schedFact = new StdSchedulerFactory();
            Scheduler sched = schedFact.getScheduler();
            sched.start();
            JobDetail job = newJob(RateUpdateJob.class)
                    .withIdentity("RATE_UPDATE_JOB", "RATE_UPDATE")
                    .build();
            Trigger trigger = newTrigger()
                    .withIdentity("RATE_UPDATE_TRIGGER", "RATE_UPDATE")
                    .withSchedule(simpleSchedule()
                            .withIntervalInSeconds(Integer.parseInt(CONFIGURATION.getValue("rate_update_interval")))
                            .repeatForever())
                    .startNow()
                    .build();

            sched.scheduleJob(job, trigger);
        } catch (Exception ex) {
            logger.error("Error in stating rate update scheduler ", ex);
        }
    }

    public void renameTable(){

    }
}
