package dao;

import beans.BankRate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.ConnUtil;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class BankRateDAOImpl implements BankRateDAO {

    private static final Logger logger = LoggerFactory.getLogger(BankRateDAOImpl.class);
    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS BANK_RATE (ID INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,BANK_NAME VARCHAR(50) NOT NULL,TENURE DECIMAL(10,2) NOT NULL,RATE_OF_INTEREST DECIMAL(10,2) NOT NULL,MIN_AMOUNT DECIMAL(10,2) NOT NULL)";
    private static final String CREATE_TABLE_DUMMY = "CREATE TABLE BANK_RATE_DUMMY (ID INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,BANK_NAME VARCHAR(50) NOT NULL,TENURE DECIMAL(10,2) NOT NULL,RATE_OF_INTEREST DECIMAL(10,2) NOT NULL,MIN_AMOUNT DECIMAL(10,2) NOT NULL)";
    private static final String SQL_SAVE = "INSERT INTO BANK_RATE_DUMMY(BANK_NAME,TENURE,RATE_OF_INTEREST,MIN_AMOUNT) VALUES(?,?,?,?)";
    private static final String SQL_GET_FOR_MIN = "SELECT BANK_NAME,TENURE,RATE_OF_INTEREST,MIN_AMOUNT FROM BANK_RATE WHERE MIN_AMOUNT >= ?";
    private static BankRateDAO bankRateDAO = new BankRateDAOImpl();
    private SQLConnectionUtil connectionUtil;

    private BankRateDAOImpl() {
        connectionUtil = SQLConnectionUtil.getInstance();
        createTable();
        createDummyTable();
    }

    public static BankRateDAO getInstance() {
        return bankRateDAO;
    }

    private void createDummyTable() {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = connectionUtil.getConnection();
            stmt = conn.createStatement();
            stmt.executeUpdate(CREATE_TABLE_DUMMY);
        } catch (SQLException ex) {
            logger.error("Error in creating db table ", ex);
        } finally {
            ConnUtil.close(conn, stmt);
        }
    }

    private void createTable() {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = connectionUtil.getConnection();
            stmt = conn.createStatement();
            stmt.executeUpdate(CREATE_TABLE);
        } catch (SQLException ex) {
            logger.error("Error in creating db table ", ex);
        } finally {
            ConnUtil.close(conn, stmt);
        }
    }

    @Override
    public void save(BankRate bankRate) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionUtil.getConnection();
            preparedStatement = connection.prepareStatement(SQL_SAVE);
            preparedStatement.setString(1, bankRate.getBankName());
            preparedStatement.setInt(2, bankRate.getMonths());
            preparedStatement.setDouble(3, bankRate.getRate());
            preparedStatement.setDouble(4, bankRate.getMinimum_amount());
            //batch update should is more desirable here
            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            logger.error("Error in executing sql query " + SQL_SAVE, ex);
        } finally {
            if (connection != null) {
                ConnUtil.close(connection, preparedStatement);
            }
        }
    }

    @Override
    public List<BankRate> getRates(double minAmount) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<BankRate> bankRates = new LinkedList<>();
        try {
            connection = connectionUtil.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_FOR_MIN);
            preparedStatement.setDouble(1, minAmount);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                addBankRate(bankRates, resultSet);
            }

        } catch (SQLException ex) {
            logger.error("Error in executing sql query " + SQL_GET_FOR_MIN, ex);
        } finally {
            if (connection != null) {
                ConnUtil.close(connection, preparedStatement);
            }
        }
        return bankRates;
    }

    @Override
    public void renameTable() {
        Connection connection = null;
        Statement stmt = null;
        List<BankRate> bankRates = new LinkedList<>();
        try {
            connection = connectionUtil.getConnection();
            stmt = connection.createStatement();
            stmt.executeUpdate("ALTER TABLE BANK_RATE RENAME TO BANK_RATE_OLD");
            stmt.executeUpdate("ALTER TABLE BANK_RATE_DUMMY RENAME TO BANK_RATE");
            stmt.executeUpdate("DROP TABLE BANK_RATE_OLD");
            createDummyTable();
        } catch (SQLException ex) {
            logger.error("Error in executing sql query " + SQL_GET_FOR_MIN, ex);
        } finally {
            if (connection != null) {
                ConnUtil.close(connection, stmt);
            }
        }
    }

    private void addBankRate(List<BankRate> bankRates, ResultSet resultSet) throws SQLException {
        String bank_name = resultSet.getString("BANK_NAME");
        int tenure = resultSet.getInt("TENURE");
        double rate_of_interest = resultSet.getDouble("RATE_OF_INTEREST");
        double min_amount = resultSet.getDouble("MIN_AMOUNT");
        BankRate bankRate = new BankRate(bank_name, tenure, rate_of_interest, min_amount);
        bankRates.add(bankRate);
    }

}
