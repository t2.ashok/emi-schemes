package dao;

import beans.BankRate;

import java.util.List;

public interface BankRateDAO {
    void save(BankRate bankRate);

    List<BankRate> getRates(double minAmount);

    void renameTable();

}
