package dao;

import org.hsqldb.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static conf.Configuration.CONFIGURATION;

public class SQLConnectionUtil {
    private static final Logger logger = LoggerFactory.getLogger(SQLConnectionUtil.class);
    private static SQLConnectionUtil sqlConnectionUtil = new SQLConnectionUtil();
    private String url;

    private SQLConnectionUtil() {
        try {
            Server server = new Server();
            String dbName = CONFIGURATION.getValue("dbName");
            server.setDatabaseName(0, dbName);
            server.setDatabasePath(0, CONFIGURATION.getValue("dbPath"));
            int port = Integer.parseInt(CONFIGURATION.getValue("dbPort"));
            server.setPort(port);
            server.start();
            url = CONFIGURATION.getValue("dbURL") + CONFIGURATION.getValue("address") + ":" + port + "/" + dbName;
            logger.info("DB URL " + url);
            Class.forName(CONFIGURATION.getValue("driverClass"));
        } catch (ClassNotFoundException Ex) {
            logger.error("Can't find db driver class");
        }
    }

    public static SQLConnectionUtil getInstance() {
        return sqlConnectionUtil;
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, CONFIGURATION.getValue("dbUsername"), "");
    }

}
